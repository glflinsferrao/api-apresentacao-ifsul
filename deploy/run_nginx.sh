#!/usr/bin/env sh
#===========================================
# remove arquivo de configuração padrão
rm -f /etc/nginx/conf.d/default.conf
#===========================================
# substitui todas as variaveis por suas respectivas variáveis de ambente
envsubst '${NGINX_SERVICE_APP}' < nginx.conf.template > /etc/nginx/conf.d/default.conf
#===========================================
#espera API subir
statusOK=200

url="$NGINX_SERVICE_APP"
curl --write-out --output /dev/null "$url"
while [ "$(curl --write-out "%{http_code}\n" --silent --output /dev/null "$url")" != $statusOK ];
do
  echo "Aguardando api subir"
  sleep 10
done
echo "API está rodando"
#===========================================
# inicia o nginx
nginx -g "daemon off;"
